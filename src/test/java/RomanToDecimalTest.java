import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RomanToDecimalTest {
    @Test
    public void addRomanNumerals() {
        Integer expected = 6;
        Integer actual = new RomanToDecimal().convert("VI");

        assertEquals(expected, actual, "Converts VI to 6");
    }

    @Test
    public void subtractRomanNumerals() {
        Integer expected = 9;
        Integer actual = new RomanToDecimal().convert("IX");

        assertEquals(expected, actual, "Converts IV to 9");
    }

    @Test
    public void addAndSubtractRomanNumerals() {
        Integer expected = 1423;
        Integer actual = new RomanToDecimal().convert("MCDXXIII");

        assertEquals(expected, actual, "Converts MCDXXIII to 1423");
    }
}
