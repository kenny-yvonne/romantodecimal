import java.util.*;

public class RomanToDecimal {
    private static Map<Character, Integer> romanToNum = new HashMap<>() {{
        put('I', 1);
        put('V', 5);
        put('X', 10);
        put('L', 50);
        put('C', 100);
        put('D', 500);
        put('M', 1000);
    }};

    public int convert(String s) {
        // s = "CXCVII"
        int runningSum = 0; // 197
        int prev = 0; // 1

        for (char c : s.toCharArray()) {
            // calculate
            int value = romanToNum.get(c); //
            if (value > prev) {
                runningSum -= prev;
                runningSum += value - prev; //
            } else {
                runningSum += value;
            }
            prev = value;
        }

        return runningSum;
    }

}
